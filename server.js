'use strict';

require('dotenv').config();
const express = require('express');
const app = express();

// const securityRoutes = require('./api/routes/security');
const topicRoutes = require('./api/routes/topic');
const mediaRoutes = require('./api/routes/media')

app
    .use(express.static(__dirname + '/public'))
    .use('/uploads', express.static('uploads'))
    .use(express.json())
    .use(express.urlencoded({extended: false}))
    .use('/api/topic', topicRoutes)
    .use('/api/upload', mediaRoutes)
    .get('/', (_, res) => res.sendFile(__dirname + 'uploads/index.html'));

//use this when any kind of error occurs
app.use((error, req, res, next) => {
	res.status(error.status || 500).json({error: {message: error.message}});
});

module.exports = app.listen(process.env.LISTEN_PORT, () => console.log('Server runnig on port ' + process.env.LISTEN_PORT));
