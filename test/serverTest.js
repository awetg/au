'use strict';

const app = require('../server');
const request = require('supertest');
// const expect = require('chai').expect; // chai expect will be used for more complex assertions

describe('GET /', () => {
    it('Should return pulic index.html file', (done) => {
        request(app).get('/')
            .expect(200)
            .end((error, res) => {
                if (error) throw error;
                done();
            });
    });
});