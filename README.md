[![pipeline status](https://gitlab.com/awetg/au/badges/master/pipeline.svg)](https://gitlab.com/awetg/au/commits/dev)


# AutoScan Backend

## Overview
AutoScan Backend is a server app built in Nodejs(Express) to provide REST API for AutoScan Android Client.

---
## Requirements

For development and production, you will need Node.js and npm, installed in your environement.

### Node
- #### Node installation

  Just go on [official Node.js website](https://nodejs.org/) and download the installer or use your linux distro specific package manager to download it.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

If the installation was successful, you should be able to run the following command.

    $ node --version
    v8.11.3

    $ npm --version
    6.1.0

###
---

## Getting started

To get the Node server running locally.

```sh
git clone git@gitlab.com:awetg/au.git
cd node-js-sample
```

### Set up the local environment
Create a new file named `.env` and from the `.env_example` file copy and fill in environmental variables values. **DO NOT** delete the `.env_example` file.

   For example:
```
# Create .env file with this environment variables.
SERVER_KEY=this is Firebase cloud messaging server key, it is needed because the server will send notication to client through FCM
LISTEN_PORT= set up which port the server need to listen e.g 3000
```

After that run this command on project path

```sh
npm install
npm start
```

Your app should now be running on [localhost:3000](http://localhost:3000/) or which ever port you set on .env file.
###
---

## Contributors
### Gitlab users
* [awetg](https://gitlab.com/awetg)
* [bishwasshrestha](https://gitlab.com/bishwas.lab)
* [Ishup Khan](https://gitlab.com/khan.alishup)
* [Utsab Kharel](https://gitlab.com/utsabk)